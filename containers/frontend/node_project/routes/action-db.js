module.exports.load = function (serverExpress, cockroachDB) {
  const { validate } = require('jsonschema');
  const funcSchema = require('./schema/funcSchema.json');
  const check_params = require('../tools/utils.js')
  
  serverExpress.post('/registerfunc', async (req, res, next) => {
    
    if (req.body == null) return next("Error Not found content function")
    const check = validate(req.body, funcSchema);
    if (!check.valid)
      return next(check.errors.map(error => error.message));

    let { function_name, code, type, creator,params } = req.body;
    console.log(req.body)
    //check if contain params 
    if(params){
      if(type=="javascript")return next('Option Javascript with parameters not yet available')
     try {
     let checked= check_params.isArrayLetter(params.split(','))
     if(!checked) return next('Error JSON format: <params> (e.x "a,b,c,d" )')
     //Avoid duplicate variants
     array_params=params.split(',')
     unique_array_params=array_params.filter((v, i, a) => a.indexOf(v) === i);
     req.body.params=unique_array_params.join();

     let equals = check_params.compareParamsandCode(unique_array_params,code)
     if(!equals) return next('Error JSON format: the number of variables of <params> and <code> do not match')
    } catch (error) {
      console.log(error)
      return next('Error JSON format: params (e.x "a,b,c,d" )')
    }
    }
    req.body.size = ~-encodeURI(JSON.stringify(req.body)).split(/%..|./).length
    let result;
    try {
      let query= await cockroachDB.getUserbyID(creator)
      if (query.message.length == 0) return next(`Creator not exist (invalid): ${creator}`);
      result = await cockroachDB.insertFunc(req.body)
    } catch (error) {
      console.log({ error })
    }
    if (!result)
      res.send({ message: "Error Trying to registry function " + function_name, success: false })
    else res.send(result);
  });


  serverExpress.post('/registeruserfunc', async (req, res, next) => {
    
    if (req.body == null) return next("Error Not found content function")
    //Check param username
    if(req.query!==null && req.query.username){ 
      try {
        let query= await cockroachDB.getUserID(req.query.username)
        if (query.message.length == 0) return next(`Creator not exist (invalid): ${req.query.username}`);
        req.body.creator=query.message[0].user_id;
      } catch (error) {
        console.log({ error })
      }
    } 
    const check = validate(req.body, funcSchema);
    if (!check.valid)
      return next(check.errors.map(error => error.message));

    let { function_name } = req.body;
    req.body.size = ~-encodeURI(JSON.stringify(req.body)).split(/%..|./).length
    let result;
    try {
      result = await cockroachDB.insertFunc(req.body)
    } catch (error) {
      console.log({ error })
    }
    if (!result)
      res.send({ message: "Error Trying to registry function " + function_name, success: false })
    else res.send(result);
  });

  serverExpress.post('/registeruser', async (req, res, next) => {
    if (req.body == null) return next("Error Not found content function")
    let { username } = req.body;
    let result;
    try {
      result = await cockroachDB.insertUser(username)
      console.log(result)
    } catch (error) {
      console.log({ error })
    }
    if (!result)
      res.send({ message: "Error Trying to registry user " + username, success: false })
    else res.send(result);
  });

  serverExpress.get('/allusers', async (req, res, next) => {
    let result;
    try {
      result = await cockroachDB.getAllUsers()
    } catch (error) {
      console.log({ error })
    }
    if (!result) return next("Error Trying to get all users registred")
    else res.send(result);
  });

  serverExpress.get('/allresources', async (req, res, next) => {
    let result;
    try {
      result = await cockroachDB.getAllResources()
    } catch (error) {
      console.log({ error })
    }
    if (!result) return next("Error Trying to get all resources")
    else res.send(result);
  });

  serverExpress.get('/allfunc', async (req, res, next) => {
    let result;
    try {
      result = await cockroachDB.getAllFunc()
    } catch (error) {
      console.log({ error })
    }
    if (!result) return next("Error Trying to get all functions storaged")
    else res.send(result);
  });

  serverExpress.get('/allsolfunc', async (req, res, next) => {
    let result;
    try {
      result = await cockroachDB.getAllSolFunc(req.body)
    } catch (error) {
      console.log({ error })
    }
    if (!result)
      res.send({ message: "Error Trying to get all functions storaged", success: false })
    else res.send(result);
  });

  serverExpress.get('/func', async (req, res, next) => {
    let function_name = req.query.function_name
    if (!function_name) return next("It's necessary to specify a name")
    let result;
    try {
      result = await cockroachDB.getFuncByName(function_name)
      console.log({ result })
    } catch (error) {
      console.log({ error })
    }
    if (!result)
      res.send({ message: `Error Trying to get functions: ${function_name}`, success: false })
    else res.send(result);
  });
  
  serverExpress.get('/userResources', async (req, res, next) => {
    let username = req.query.username
    if (!username) return next("Username required")
    let result;
    try {
      result = await cockroachDB.getResourcesFromUser(username)
    } catch (error) {
      console.log({ error })
    }
    if (!result)
      res.send({ message: "Error Trying to get user resources", success: false })
    else res.send(result);
  });
  serverExpress.get('/solfuncs', async (req, res, next) => {
    let username = req.query.username
    if (!username) return next("Username required")
    let result;
    try {
      result = await cockroachDB.getSolFuncByNameUser(username)
    } catch (error) {
      console.log({ error })
    }
    if (!result)
      res.send({ message: "Error Trying to get user solutions", success: false })
    else res.send(result);
  });
  serverExpress.get('/funcSolution', async (req, res, next) => {
    let username = req.query.username
    let function_name = req.query.function_name
    if (!username || !function_name) return next("Username and function name required")
    let result;
    try {
      result = await cockroachDB.getResultFromFunction(username, function_name)
    } catch (error) {
      console.log({ error })
    }
    if (!result)
      res.send({ message: `Error Trying to get function solution from function: ${function_name}`, success: false })
    else res.send(result);
  });

}
