package db

import k "kumori.systems/kumori/kmv"

#Manifest: k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.minifaas"
    name: "faas_db"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: dbserver: {
        protocol: "tcp"
        port:     26257
      }
      duplex: maindbcluster: {
        protocol: "tcp"
        port:     26257
      }
     
    }

    config: {
      resource: {}
      parameter: {}
    }

    size: {
      $_memory: *"12000Mi" | uint
      $_cpu: *"500m" | uint
    }

    code: db: k.#Container & {
      name: "db"
      entrypoint: ["initdb.sh"]
      image: {
        hub: {
          name: "registry.hub.docker.com"
          secret: ""
        }
        tag: "mamarbao/faas-db:2.4"
      }
      mapping: {
        filesystem: []
        env: {
          DBSERVER_PORT_ENV: "\(srv.server.dbserver.port)",
          COCKROACH_HOST1: "0.0.0.0",
          WAIT:"50",
          CHANNEL:"maindbcluster",
          INSTANCE:"3"
        }
      }
    }
  }
}

