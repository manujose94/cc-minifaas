var instance = null
const statsQueue = require('./queue');
const { validate } = require('jsonschema');
const statsSchema = require('./schema/statsSchema.json');
/**
    Class that has as a main function, to obtain 
    statistics of the requests directed to the workers 
    or to the registration of new functions
 */
class WatchMan {

    constructor() {
        this.frame_stats = new statsQueue();
        this.frame_stats.setMaxlen(10)
        this.totalRequests = 0; // total incoming requests
    }

    getTotalRequest() {
        return this.totalRequests;
    }

    getAvarageTime() {
        return this.frame_stats.getAvarageTime()
    }

    showStats() {
        this.frame_stats.show()
    }

    getStats() {
        return { average_time: this.frame_stats.getAvarageTime() , current_frame: this.frame_stats}
    }

    saveStats(stat) {
        this.totalRequests++;
        let check = validate(stat, statsSchema);
        if (!check.valid) {
            console.error('[WM] Invalid JSON Format', check.errors.map(error => error.message))
        } else {
            this.frame_stats.enqueue(stat)
            console.log(this.frame_stats.getAvarageTime())
        }
    }


    static getInstance(EventEmitter) {
        if (!instance) {
            instance = new WatchMan()
        }

        return instance
    }
}
module.exports.instance = () => {
    return WatchMan.getInstance();
}
