#!/bin/sh
set -eu
#Little wait via sleep
sleep $WAIT
myip=$(hostname -i)
nodo=${HOSTNAME:${#HOSTNAME}-1:1}
#Get Route IPS  via port 4248
myroutes=$(nslookup $CHANNEL | awk '/Address: 1/{print}'| sed 's"Address: "nats://"' | tail -n $INSTANCES | awk '{print}' ORS=':4248,' | sed 's/.$//')
echo "[0] CH: $CHANNEL INS: $INSTANCES IPS: $myip  IP_HOST: $myip Nodo: $nodo"
#-DV MODE DEBUG
nats-server -p 4222 -cluster nats://$myip:4248 -routes $myroutes -DV
#router: Routes to solicit and connect 
#cluster: Cluster URL for solicited routes