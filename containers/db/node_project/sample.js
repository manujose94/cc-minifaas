//Using the pg driver helps the constant variable to communicate to the CockroachDB
var async = require("async");
var fs = require("fs");
var pg = require("pg");

// Connection to the database
var config = {
  user: "test",
  host: "localhost",
  database: "test",
  port: 26257
};

// Pass the configurations of the pool to the client once its created
var pool = new pg.Pool(config);
pool.connect(function(err, client, done) {
  // Close the connection to the database and exit
  var finish = function() {
    done();
    process.exit();
  };

  if (err) {
    console.error("Error connecting to the CockroachDB", err);
    finish();
  }

  // async.waterfall is used to run a multiple task that is dependent to the previous one.
  async.waterfall(
    [
      function(next) {
        // Create the an 'demo' table
        let col_query="(id INT PRIMARY KEY DEFAULT unique_rowid(),"+
                        "name_function TEXT NOT NULL,"+
                        "name_user TEXT NOT NULL,"+
                        "code_function TEXT NOT NULL);"
        let col_query2="(id INT PRIMARY KEY DEFAULT unique_rowid(),"+
                        "id_functions INT,"+
                        "result TEXT NOT NULL,"+
                        "date TIMESTAMP(3) );" //format for dates is YYYY-MM-DD
        client.query(
          "CREATE TABLE IF NOT EXISTS functions "+col_query+" CREATE TABLE IF NOT EXISTS results "+col_query2,
          next
        );
       
      },
      function(results, next) {
        // Insert three rows into the 'demo' table.
        client.query(
          "INSERT INTO functions( name_function,code_function,name_user) VALUES ('suma', 'console.log(3+4)','halo'), ('resta', 'console.log(3-4)','halo')",
          next
        );
      },
      function(results, next) {
        // Print the record inserted into the table
        client.query("SELECT * FROM functions;", next);
      }
    ],
    function(err, results) {
      if (err) {
        console.error("Error Inserting and Printing demo table: ", err);
        finish();
      }

      console.log("Sample Node with CockroachDB:");
      results.rows.forEach(function(row) {
        console.log(row);
      });

      finish();
    }
  );
});