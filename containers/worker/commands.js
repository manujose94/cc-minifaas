function execShellCommandTimeout(cmd,_delay=5000) {
    const exec = require('child_process').exec;
    return  Promise.race([new Promise((resolve, reject) => {
       exec(cmd, (error, stdout, stderr) => {
          if (error){reject(error); console.log({error})};
          resolve(stdout ? stdout : stderr);
       });
    }),new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), _delay))
  
  ]);
}

async function create_and_launch(name_function,code_function,_delay) {
    if(!code_function || !name_function) return {message: message, success: false}
    try{
       let path =await 
       execShellCommandTimeout("pwd")
       console.log({path})
       result = await 
                execShellCommandTimeout(`./create_and_launch.sh ${name_function} "${code_function}"`,_delay);
    }catch(error) {
       console.warn("error",error.message)
       let {message} = error;
       return {message: "failed to launch function JS", success: false}
    }
    console.log("[localCommand result successfully]",{result});
    
    return {message: result, success: true};
   
    
  }
  exports.create_and_launch = create_and_launch;