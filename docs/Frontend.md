# Frontend

Componente encargado de proporcionar interacción con el cliente y ofrecer las funcionalidades y servicios del sistema Faas implementado. En rasgo generales, consta de un servidor HTTP Rest que hace uso de una serie de librerías para comunicarse bajo Nats y manejar la persistencia de datos mediante el uso de una base de datos CockroachDB. Todo ello monitorzado por el componente Watchman.

### Implementación

Se ha implementado bajo el lenguaje nodejs juntos a una serie de paquetes esenciales:

- express
- nats
- pg
- jsonschema

### Funcionalidades

- **Permitir solicitudes** bajo peticiones HTTP REST

  - En el documento [Request](./Resquests.md) se detallan todas las peticiones disponibles.

- **Conexión y manejo de la base de datos**

  - Mantener una conexión Pool con la base de datos, de esta forma que se mantiene solo una conexión activa por cada instancia del frontend.
  - Al iniciar el frontend, se inicializa la base de datos y se generan los siguientes datos:
    - Usuario *mamarbao* de tipo invitado
    - Usuario *manager* de tipo admin
    - Función *suma* de tipo javascript asignada al usuario mamarbao
    - Función *resta* de tipo maths asignada al usuario mamarbao
  - Nota:  Estos datos son creados solo una vez, solo sino existen. 

- **Conectividad con Nats**

  - Se realizan una serie de requests con la siguiente configuración:
    - En la request solo se acepta una respuesta, después es eliminada
    - La request contiene un timeout en caso de que la respuesta por parte de un Worker exceda un tiempo especificado.
    - Las request implementadas tiene como función:
    - Enviar la función solicitada para ser procesada.
    - Enviar los stats (estadísticas) de los lanzamiento que se están llevando acabo
    - Petición de los actuales stats del sistema.

- **Detección de errores**

  - En caso de cualquier error, es emitido un mensaje de la posible causa
  - Se comprueba que los datos emitidos mediante json, siguen un esquema predefinido.
  - Antes de iniciar el servidor HTTP, se comprueba que tanto la conectividad a Nats como a la base de datos ha sido establecida.

- **Control de recursos**

  Las respuestas generadas por la petición de lanzamiento de una función, son enviadas al componentes **watchman** antes de ser reenviado al cliente, con los siguiente datos:
  
  ```shell
   {
              "method": "GET",
              "originalUrl": "/launch?name_function=resta&username=mamarbao",
              "duration": 42.534485,
              "time_scale": "ms",
              "size": 12,
              "instance": "kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572",
              "success": false
  }
  ```
  
