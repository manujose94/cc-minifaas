## Peticiones miniFaas

*Certificado expirado*

```json
Certificate CN: *.vera.kumori.cloud
validTo: Jan 24 15:42:21 2021 GMT
authorizationError: "CERT_HAS_EXPIRED"
```
En caso de estar el certificado expirado añadir a **curl** el parámetro ***-k***:
```
curl -k --location --request
```

**Tabla de contenido**

- [Peticiones miniFaas](#peticiones-minifaas)
- [Requests](#requests)
  - [Usuarios](#usuarios)
    - [Registrar Usuario](#registrar-usuario)
    - [Obtener todos los Usuarios](#obtener-todos-los-usuarios)
  - [Funciones](#funciones)
    - [Registrar función](#registrar-función)
      - [Función Javascript sin parámetros con id usuarios](#función-javascript-sin-parámetros-con-id-usuarios)
      - [Función Javascript sin parámetros  vía username](#función-javascript-sin-parámetros--vía-username)
      - [Registro de una función maths con parámetros](#registro-de-una-función-maths-con-parámetros)
    - [Lanzar función registrada](#lanzar-función-registrada)
    - [Lanzar función registrada con parámetros](#lanzar-función-registrada-con-parámetros)
    - [Obtener Soluciones de las  Funciones](#obtener-soluciones-de-las--funciones)
    - [Soluciones de Funciones de un Usuario](#soluciones-de-funciones-de-un-usuario)
  - [Registros](#registros)
    - [Obtener todos los registros](#obtener-todos-los-registros)
    - [Registros generados por un usuario](#registros-generados-por-un-usuario)
  - [Stats](#stats)
    - [Obtener los stats actuales](#obtener-los-stats-actuales)
- [Manejar Instancias (worker,frontend,db,nats,watchman)](#manejar-instancias-workerfrontenddbnatswatchman)
  - [Instanciar un componente](#instanciar-un-componente)
    - [Worker](#worker)
    - [Frontend](#frontend)
    - [Especificando número de instancias](#especificando-número-de-instancias)
  - [Eliminar una Instancia](#eliminar-una-instancia)
    - [Worker](#worker-1)
    - [Obtener Resultado de la actualización de instancias](#obtener-resultado-de-la-actualización-de-instancias)



## Requests

### Usuarios

------



#### Registrar Usuario

```shell
curl --location --request POST 'https://minifaas-cm2.vera.kumori.cloud/registeruser' \
--header 'Content-Type: application/json' \
--data-raw '{
  "username": "catarina"
}'
```
**Salida exitosa**

```json
{
    "message": "Inserted correctly",
    "success": true,
    "creator": "629587467609997313"
}
```
Si usuario ya existe
```json
{
    "message": "User already exist",
    "success": false
}
```

#### Obtener todos los Usuarios

```shell
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/allusers'
```
**Salida exitosa**

```shell
{
    "message": [
        {
            "user_id": "628909688570445825",
            "username": "mamarbao",
            "user_rol": "user"
        },
        {
            "user_id": "628909767323320321",
            "username": "catarina",
            "user_rol": "user"
        }
    ],
    "success": true
}
```

### Funciones

------

Note: Las funciones registradas solo pueden ser lanzadas por el usuario que las ha registrado.

#### Registrar función

##### Función Javascript sin parámetros con id usuarios

```shell
curl --location --request POST 'https://minifaas-cm2.vera.kumori.cloud/registerfunc' \
--header 'Content-Type: application/json' \
--data-raw '{
            "type": "javascript",
            "function_name": "partepastel",
            "code": "let pastel=10000000;let repartir=5; console.log(pastel/repartir)"
}'
```
##### Función Javascript sin parámetros  vía username
```shell
curl --location --request POST 'https://minifaas-cm2.vera.kumori.cloud/registeruserfunc?username=mamarbao' \
--header 'Content-Type: application/json' \
--data-raw '{
            "type": "javascript",
            "function_name": "fibonacci",
            "code": "function fibonacci(num){var a = 1, b = 0, temp;while (num >= 0){temp = a;a = a + b;b = temp;num--;}return b;};console.log(fibonacci(24))"
}'

```
**Salida exitosa**

```shell
{
    "message": "Inserted correctly",
    "success": true
}
```

**Salida no exitosa**

​	Id de creados no valida

```shell
{
    "error": "Creator not exist (invalid): 631844972575227905",
    "success": false
}
```
​	Error Interno
```shell
{
    "message": "Error Trying to registry function divisionGrande",
    "success": false
}
```

##### Registro de una función maths con parámetros

```bash
curl --location --request POST 'https://minifaas-cm2.vera.kumori.cloud/registerfunc' \
--header 'Content-Type: application/json' \
--data-raw '{
            "type": "maths",
            "function_name": "areaTrapecio",
            "creator": "631844972575227905",
            "code": "((b+a)/2)*h",
            "params": "a,b,h"
}'
```

**Salida exitosa**

```json
{
    "message": "Inserted correctly",
    "success": true
}
```

**Salidas erróneas**

​	Tipo Incorrecto:

```json
{
    "error": "Option Javascript with parameters not yet available",
    "success": false
}
```

​	Valor params con mal formato (*ex params: "a,,b,h"*)

```json
{
    "error": "Error JSON format: params (e.x \"a,b,c,d\" )",
    "success": false
}
```

​	Las variables del código y de los parámetros no coincide 

> Ex: code: ""((b+a)/2)*h"  not match with params: "a,b"

```json
{
    "error": "Error JSON format: the number of variables of <params> and <code> do not match",
    "success": false
}
```



#### Lanzar función registrada

```shell
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/launch?name_function=divisionGrande&username=mamarbao' \
--data-raw ''
```

**Salida Exitosa**

```shell
{
    "message": "5000000\n",
    "success": true,
    "duration": 67.58409,
    "function_id": "628473839537651713",
    "instance": "kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572",
    "type": "javascript"
}
```

**Salida no Exitosa**

```json
{
    "error": "Not Exist Function name: divisionGrande23",
    "success": false
}
```

#### Lanzar función registrada con parámetros

```bash
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/launch?name_function=areaTrapecio&username=mamarbao&params_value=12,15,6' \
--data-raw ''
```
**Salida exitosa**

​	Todos los parámetros indicados

```json
{
    "message": 81,
    "duration": 2.950773,
    "function_id": "631855982230732801",
    "success": true,
    "instance": "kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572",
    "type": "maths"
}
```

​	Falta algún parámetro

> En este caso son necesarios 3 (a,b,h) pero se deja por declarar h

```bash
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/launch?name_function=areaTrapecio&username=mamarbao&params_value=12,15'
```

```json
{
    "message": {
        "mathjs": "Unit",
        "value": 13.5,
        "unit": "h",
        "fixPrefix": false
    },
    "duration": 3.880296,
    "function_id": "631855982230732801",
    "success": true,
    "instance": "unknown",
    "type": "maths"
}
```

**Salida errónea**

​	Faltan parámetros:

```json
{
    "error": {
        "error": "Insufficient parameters, must give value to: areaTrapecio",
        "message": " params: a,b,h -> (((b+a)/2)*h)"
    },
    "success": false
}
```


#### Obtener Soluciones de las  Funciones

```shell
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/allsolfunc'
```

**Salida exitosa**

```shell
{
    "message": [
        {
            "res_id": "628467143079100417",
            "function_id": "200",
            "result": "7\n",
            "date": "2021-01-28T07:49:11.000Z"
        },
        {
            "res_id": "628474320478404609",
            "function_id": "628473839537651713",
            "result": "5000000\n",
            "date": "2021-01-28T08:25:41.000Z"
        }
    ],
    "success": true
}
```

#### Soluciones de Funciones de un Usuario

```shell
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/solfuncs?username=mamarbao'
```

**Salida exitosa**

```json
{
    "message": [
        {
            "result": "7\n",
            "date": "2021-01-28T07:50:56.000Z",
            "function_name": "suma",
            "username": "mamarbao"
        },
        {
            "result": "5000000\n",
            "date": "2021-01-28T08:25:41.000Z",
            "function_name": "divisionGrande",
            "username": "mamarbao"
        }
    ],
    "success": true
}
```

### Registros

------



#### Obtener todos los registros

```shell
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/allresources'
```

**Salida exitosa**

```json
{
    "message": [
        {
            "reg_id": "628467486882955265",
            "user_id": "628207816724643841",
            "function_id": "200",
            "use_time": 56.399426,
            "instance_type": "kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572",
            "date": "2021-01-28T07:50:56.000Z"
        },
        {
            "reg_id": "628474320506126337",
            "user_id": "628207816724643841",
            "function_id": "628473839537651713",
            "use_time": 67.58409,
            "instance_type": "kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572",
            "date": "2021-01-28T08:25:41.000Z"
        }
    ],
    "success": true
}
```

#### Registros generados por un usuario

```shell
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/userResources?username=mamarbao'
```

**Salida exitosa**

```shell
{
    "message": [
        {
            "function_name": "suma",
            "size": "500",
            "use_time": 59.496182,
            "instance_type": "0"
        },
        {
            "function_name": "divisionGrande",
            "size": "118",
            "use_time": 67.58409,
            "instance_type": "0"
        }
    ],
    "success": true
}
```

### Stats 

------



#### Obtener los stats actuales

```shell
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/currentstats'
```

```json
{
    "_maxlen": 10,
    "_items": [
        {
            "method": "GET",
            "originalUrl": "/launch?name_function=suma&username=mamarbao",
            "duration": 103.171684,
            "time_scale": "ms",
            "size": 18,
            "instance": "kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572",
            "success": true
        },
        {
            "method": "GET",
            "originalUrl": "/launch?name_function=suma&username=mamarbao",
            "duration": 77.431302,
            "time_scale": "ms",
            "size": 18,
            "instance": "kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572",
            "success": true
        },
        {
            "method": "GET",
            "originalUrl": "/launch?name_function=resta&username=mamarbao",
            "duration": 42.534485,
            "time_scale": "ms",
            "size": 12,
            "instance": "kd-101844-029c82ba-worker000-deployment-5cfc9d6ddb-lc572",
            "success": false
        }
    ]
}
```

**Salidas erróneas**

​	No especificar usuario

```json
{
   "error": "It's necessary to specify a username",
    "success": false
}
```

​	Usuario sin permisos suficientes

```json
{
    "error": "Insufficient permissions for action",
    "success": false
}
```



## Manejar Instancias (worker,frontend,db,nats,watchman)

Parámetros

- decision: [kill,create]
- username: text // *e.x manager - rol like admin*
- type: [worker,frontend,db,nats,watchman]
- instances: number //*Num of instances that we want add (only create)*

### Instanciar un componente

------



#### Worker
```bash
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/takedecision?decision=create&username=manager&type=worker'
```
#### Frontend

```bash
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/takedecision?decision=create&username=manager&type=frontend'
```

#### Especificando número de instancias

```bash
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/takedecision?decision=create&username=manager&type=worker&instances=2'
```
```json
{
    "uuid_operation": "24d79190-698b-11eb-a332-c56af1b8a5c3",
    "success": true,
    "message": "Successful operation: create. It may take some time to establish itself, wait",
    "current_instances": "description: role: worker: rsize: $_instances: 3\n",
    "updated_to_instances": "5\n",
    "type": "worker"
}
```

### Eliminar una Instancia

------

Limitada por seguridad a 1

#### Worker
```bash
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/takedecision?decision=kill&username=manager&type=worker'
```

**Salida exitosa**

```json
{
    "uuid_operation": "24d79190-698b-11eb-a772-c56af1b8a5c3",
    "success": true,
    "message": "Successful operation: create. It may take some time to establish itself, wait",
    "current_instances": "description: role: worker: rsize: $_instances: 3\n",
    "updated_to_instances": "4\n",
    "type": "worker"
}
```

**Salida errónea** (Los tres casos)

​	Timeout
```json
{
 "success": false,
 "message": "Function (requestDecision) timed out"
}
```
​	Internal error
```json
{
  "success": false,
  "message": "failed to update desployment kumori"
}
```

**Falta de algún parámetro:**

Se valida mediante un *jsonchema* tanto en el **frontend** como en el componente **watchman**:

Ejemplo: 

```bash
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/takedecision?decision=create'
```

```json
{
    "errors": [
        "requires property \"username\"",
        "requires property \"type\""
    ],
    "success": false
}
```

#### Obtener Resultado de la actualización de instancias

Mediante el UUID que nos retorna, si la petición se ha realizado correctamente:

```bash
curl --location --request GET 'https://minifaas-cm2.vera.kumori.cloud/soldecision?uuid=24d79190-698b-11eb-a772-c56af1b8a5c3'
```
**Salida Exitosa**
```json
{
    "message": "spawn kumorictl login mamarbao\r\nEnter Password: \r\nLogin OK.\r\nDeployment minifaas-cm2/faas-deploy updated\n",
    "success": true,
    "changes": {
        "message": "4\n",
        "success": true,
        "current_instances": "description: role: worker: rsize: $_instances: 3\n",
        "instances": "4\n",
        "type": "worker"
    }
}
```

Nota: Se ha realizado siguiendo esta metodología, debido a que el comando *kumrictl update*, puede demorarse bastante tiempo y por tanto, la request puede ser cancelada por el cliente que realiza la petición.

